namespace CommonsHelper
{

	public enum CardinalDirection
	{
		Down = 0,
		Left = 1,
		Up = 2,
		Right = 3
	}

}

