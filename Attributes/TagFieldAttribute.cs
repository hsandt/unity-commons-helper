﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CommonsHelper
{
	/// Attribute to select a tag amont the list of tags defined at edit time
	public class TagFieldAttribute : PropertyAttribute
	{
	}
}
