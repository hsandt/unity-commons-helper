﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CommonsHelper
{
	/// Attribute to select a layer amont the list of layers defined at edit time
	public class LayerFieldAttribute : PropertyAttribute
	{
	}
}
